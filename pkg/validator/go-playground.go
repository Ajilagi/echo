package validator

import (
	validator "github.com/go-playground/validator/v10"
)

type CustomValidator struct {
	Validator *validator.Validate
}

func (cv CustomValidator) Validate(mystruct interface{}) error {
	err := cv.Validator.Struct(mystruct)
	if err != nil {
		return err
	}

	return nil
}