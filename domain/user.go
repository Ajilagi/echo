package domain

type UserByIDReq struct {
	ID   int    `param:"id" json:",omitempty" validate:"required"`
	Name string `json:"name" `
	Age  int    `json:"age"`
}

type UserCreateReq struct {
	ID   int    `param:"id" json:",omitempty"`
	Name string `json:"name" validate:"required"`
	Age  int    `json:"age" validate:"required,gte=17"`
}
