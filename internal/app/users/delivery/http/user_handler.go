package http

import (
	"log"
	"net/http"

	"bitbucket.org/Ajilagi/echo/domain"
	"github.com/labstack/echo/v4"
)

type UserHandler struct {
	e *echo.Echo
}

func NewUserHandler(e *echo.Echo) {
	userHandler := &UserHandler{
		e: e,
	}

	groupUsers := e.Group("/users")
	groupUsers.GET("", userHandler.GetAllUser)
	groupUsers.GET("/:id", userHandler.GetUserByID)
	groupUsers.POST("", userHandler.CreateUser)
	groupUsers.PATCH("/:id", userHandler.UpdateUser)
	groupUsers.DELETE("/:id", userHandler.DeleteUser)
}

func (handler *UserHandler) GetAllUser(c echo.Context) error {
	return c.String(http.StatusOK, "Hello Get Users!")
}

func (handler *UserHandler) GetUserByID(c echo.Context) error {
	user := new(domain.UserByIDReq)
	if err := c.Bind(user); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	if err := c.Validate(user); err != nil {
		log.Println("error validation : ", err.Error())
		log.Println(user)
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	return c.JSON(http.StatusOK, user)
}

func (handler *UserHandler) CreateUser(c echo.Context) error {
	user := new(domain.UserCreateReq)
	if err := c.Bind(user); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	if err := c.Validate(user); err != nil {
		log.Println("error validation : ", err.Error())
		log.Println(user)
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	return c.JSON(http.StatusCreated, user)
}

func (handler *UserHandler) UpdateUser(c echo.Context) error {
	user := new(domain.UserCreateReq)
	if err := c.Bind(user); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	if err := c.Validate(user); err != nil {
		log.Println("error validation : ", err.Error())
		log.Println(user)
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	return c.JSON(http.StatusCreated, user)
}

func (handler *UserHandler) DeleteUser(c echo.Context) error {
	user := new(domain.UserByIDReq)
	if err := c.Bind(user); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	if err := c.Validate(user); err != nil {
		log.Println("error validation : ", err.Error())
		log.Println(user)
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	return c.String(http.StatusOK, "Hello Delete User!")
}
