package main

import (
	"errors"
	"os"

	userHttp "bitbucket.org/Ajilagi/echo/internal/app/users/delivery/http"
	pkgvalidator "bitbucket.org/Ajilagi/echo/pkg/validator"
	validator "github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	e := echo.New()
	e.Validator = pkgvalidator.CustomValidator{
		Validator: validator.New(),
	}

	e.Use(middleware.CORS())
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{}))
	e.Use(AuthAPIKey)

	userHttp.NewUserHandler(e)

	e.Logger.Fatal(e.Start(":" + os.Getenv("PORTECHO")))
}

func AuthAPIKey(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ec echo.Context) error {
		apiKey := ec.Request().Header.Get("api-key")
		if apiKey != "secretApiKey" {
			ec.Error(errors.New("invalid api-key"))
		}

		return next(ec)
	}
}
